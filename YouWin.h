#pragma once
#include "cocos2d.h"
#include "FirstLevel.h"
class YouWin : public cocos2d::Scene {

private:
	int _winScores = 0;
	cocos2d::Label* _winScoresLabel;

public:

	static cocos2d::Scene* createScene();

	virtual bool init();

	void restartCallback(cocos2d::Ref* pSender);

	void quitGameCallback(cocos2d::Ref* pSender);

	void setWinScoresLabel(int& scores);

	CREATE_FUNC(YouWin);

};
