#pragma once
#include "cocos2d.h"

class Block : public cocos2d::Sprite {

public:

	Block() : cocos2d::Sprite() {

		lives = cocos2d::RandomHelper::random_int(1, 3);
		CCLOG("CREATE BLOCK");
	}

	static Block* createBlock();

	void setTexture(const std::string& filename);

	int getLives();

	void setLives(int);


private:

	int lives;

public:

	std::vector<std::string> blockPictures = { "Arcanoid/grey.png", "Arcanoid/green.png", "Arcanoid/red.png", "Arcanoid/blue.png" };

	CREATE_FUNC(Block);

};