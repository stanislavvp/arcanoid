#include "Block.h"

int Block::getLives() {
	return this->lives;
}

void Block::setLives(int howLives) {
	this->lives = howLives;
}


Block* Block::createBlock()
{
	Block* block = new (std::nothrow) Block();
	if (block && block->initWithFile(block->blockPictures[block->getLives()]))
	{
		block->autorelease();
		return block;
	}
	CC_SAFE_DELETE(block);
	return nullptr;
}

void Block::setTexture(const std::string& filename)
{
	cocos2d::Sprite::setTexture(filename);
}