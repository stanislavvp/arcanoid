#pragma once
#include "cocos2d.h"
#include "FirstLevel.h"
class GameOver : public cocos2d::Scene {

private:
	int _overScores = 0;
	cocos2d::Label * _overScoresLabel;

public:

	static cocos2d::Scene* createScene();

	virtual bool init();

	void restartCallback(cocos2d::Ref* pSender);

	void quitGameCallback(cocos2d::Ref* pSender);

	void setOverScoresLabel(int& scores);

	CREATE_FUNC(GameOver);

};