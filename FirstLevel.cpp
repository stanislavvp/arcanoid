#include "FirstLevel.h"
#include "ui/CocosGUI.h"
#include "Block.h"
#include "physics/CCPhysicsBody.h"
#include "base/CCVector.h"
#include "GameOver.h"
#include "YouWin.h"
#include "GameMenu.h"
#include <string>
#include "toString.h"

USING_NS_CC;

Scene* FirstLevel::createScene()
{
	auto scene = Scene::createWithPhysics();
	// drawing borders of objects
	//scene->getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);

	// create layer
	auto layer = FirstLevel::create();
	// add physics to layer
	layer->SetPhysicsWorld(scene->getPhysicsWorld());

	//add layer to scene
	scene->addChild(layer);

	return scene;

}

bool FirstLevel::init()
{

	if (!Scene::initWithPhysics())
	{
		return false;
	}

	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	//screen border
	auto borderBody = PhysicsBody::createEdgeBox(visibleSize, PhysicsMaterial(1.0f, 1.0f, 0.0f), 1);
	auto borderNode = Node::create();
	borderNode->setPosition(Point(visibleSize.width / 2, visibleSize.height / 2));
	borderNode->setPhysicsBody(borderBody);
	this->addChild(borderNode, 1);

	//background
	auto background = DrawNode::create();
	background->drawSolidRect(origin, visibleSize, Color4F(0.0, 0.0, 0.0, 1.0));
	this->addChild(background, 0);
	//Scores Label
	_scoresLabel = Label::createWithTTF(makeScoresLabel(this->_scores), "fonts/STARWARS.ttf", 30);
	_scoresLabel->setTextColor(Color4B::YELLOW);
	_scoresLabel->setPosition(Vec2(visibleSize.width / 2, visibleSize.height / 2));
	this->addChild(_scoresLabel, 1);

	//player sprite

	//create phisics body for player 	
	_playerSprite = Sprite::create("Arcanoid/paddleBlu.png");
        _playerSprite->setScale(2.0);
	auto playerBody = PhysicsBody::createBox(Size(_playerSprite->getContentSize().width, _playerSprite->getContentSize().height),
		PhysicsMaterial(0.0f, 1.0f, 0.0f));
	playerBody->setDynamic(false);
	//set gravity for player sprite to null
	playerBody->setGravityEnable(false);
	_playerSprite->setAnchorPoint(Vec2(0.5, 0.5));
	_playerSprite->setPosition(Vec2(visibleSize.width / 2 + origin.x, visibleSize.height / 10 + origin.y));
	this->addChild(_playerSprite, 1);
	//add physicsBodey to player sprite
	_playerSprite->addComponent(playerBody);

	//create block sprites with physics bodyes
	Vector<Block*>allBlocks;
	for (int blockIndex = 0; blockIndex < 75; ++blockIndex) {
		Block* blockObject = NULL;

		blockObject = Block::createBlock();

		blockObject->setContentSize(Size(visibleSize.width / 15, visibleSize.height / 30));
		//create phisics body for blocks
		auto blockBody = PhysicsBody::createBox(Size(visibleSize.width / 15, visibleSize.height / 30),
			PhysicsMaterial(0.0f, 1.0f, 0.0f));
		//disable moving of blocks
		blockBody->setDynamic(false);
		//set gravity to block sprite to null
		blockBody->setGravityEnable(false);
		blockObject->setAnchorPoint(Vec2(0, 1));
		//add physicsBodey to block sprite
		blockObject->setPhysicsBody(blockBody);
		//set bitmask for blockbody
		blockBody->setCollisionBitmask(1);
		blockBody->setContactTestBitmask(true);
		// push block object to vector
		allBlocks.pushBack(blockObject);
	}
	//set position of blocks
	auto iterBlock = allBlocks.begin();
	for (int fieldHeight = 0; fieldHeight < 5; ++fieldHeight) {

		for (int fieldWidth = 0; fieldWidth < 15; ++fieldWidth) {

			auto blockWidth = (*iterBlock)->getContentSize().width;
			auto blockHeight = (*iterBlock)->getContentSize().height;
			origin.x = blockWidth*fieldWidth;
			origin.y = blockHeight*fieldHeight;
			(*iterBlock)->setPosition(Vec2(origin.x, visibleSize.height - origin.y));
			this->addChild(*iterBlock, 1);

			if (iterBlock != allBlocks.end()) {
				++iterBlock;
			}
		}

	}
	//floor sprite
	auto floor = Sprite::create("Arcanoid/grey.png");
	floor->setAnchorPoint(Vec2(0.5, 0.5));
	floor->setContentSize(Size(visibleSize.width, visibleSize.height / 30));
	auto floorBody = PhysicsBody::createBox(Size(visibleSize.width, visibleSize.height / 30),
		PhysicsMaterial(0.0f, 1.0f, 0.0f));
	floorBody->setDynamic(false);
	floorBody->setGravityEnable(false);
	floor->setPhysicsBody(floorBody);
	floorBody->setCollisionBitmask(3);
	floorBody->setContactTestBitmask(true);
	floor->setPosition(Vec2(visibleSize.width / 2, visibleSize.height / 60));
	this->addChild(floor, 1);

	//ball sprite
	auto ball = Sprite::create("Arcanoid/ball.png");
        ball->setScale(2.0);
	ball->setAnchorPoint(Vec2(0.5, 0.5));
	auto ballBody = PhysicsBody::createCircle(ball->getContentSize().width / 2, PhysicsMaterial(0.0f, 1.0f, 0.0f));
	ball->setPhysicsBody(ballBody);
	//set bitmask for ballbody
	ballBody->setCollisionBitmask(2);
	ballBody->setContactTestBitmask(true);
	ball->setPosition(Vec2(_playerSprite->getPosition().x, _playerSprite->getPosition().y + ball->getContentSize().width * 2));
	this->addChild(ball, 1);
	//disable gravity for ball
	ball->getPhysicsBody()->setGravityEnable(false);
	//disable angular velocity
	ballBody->setAngularVelocity(0.0f);
	//set start velocity for ball
	ball->getPhysicsBody()->setVelocity(Vec2(130, 130));
	//handling of collisions between ball and blocks
	auto contactListener = EventListenerPhysicsContact::create();
	contactListener->onContactSeparate = CC_CALLBACK_1(FirstLevel::onContactSeparate, this);
	contactListener->onContactBegin = CC_CALLBACK_1(FirstLevel::onContactBegin, this);
	this->getEventDispatcher()->addEventListenerWithSceneGraphPriority(contactListener, this);

	//touch handling
	auto touchListener = EventListenerTouchOneByOne::create();
	touchListener->setSwallowTouches(true);
	touchListener->onTouchBegan = CC_CALLBACK_2(FirstLevel::onTouchBegan, this);
	touchListener->onTouchMoved = CC_CALLBACK_2(FirstLevel::onTouchMoved, this);
	touchListener->onTouchEnded = CC_CALLBACK_2(FirstLevel::onTouchEnded, this);
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(touchListener, this);

	return true;
}

bool FirstLevel::onContactBegin(PhysicsContact &contact) {

	PhysicsBody* contactBallBody = contact.getShapeA()->getBody();
	PhysicsBody* contactBlockBody = contact.getShapeB()->getBody();
	//save velocity in contact moment
	float *velocity = new float[2];
	velocity[0] = contactBallBody->getVelocity().length();
	velocity[1] = contactBlockBody->getVelocity().length();
	contact.setData(velocity);
	//cotact with floor - start GameOver scene
	if ((3 == contactBallBody->getCollisionBitmask() && 2 == contactBlockBody->getCollisionBitmask()) ||
		(2 == contactBallBody->getCollisionBitmask() && 3 == contactBlockBody->getCollisionBitmask())) {
		CCLOG("COLLISION WITH FLOOR");
		auto gameOver = GameOver::createScene();
		//set scores label in game over scene
		GameOver* over = dynamic_cast<GameOver*>(gameOver);
		over->setOverScoresLabel(_scores);
		Director::getInstance()->replaceScene(TransitionFade::create(3.5, gameOver, Color3B(255, 0, 0)));
	}

	return true;
}

bool FirstLevel::onContactSeparate(PhysicsContact& contact) {

	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	PhysicsBody* contactBallBody = contact.getShapeA()->getBody();
	PhysicsBody* contactBlockBody = contact.getShapeB()->getBody();
	//keep direction and restore velocity of ball
	float *velocity = (float*)contact.getData();
	auto ballVelocity = contactBallBody->getVelocity();
	auto blockVelocity = contactBlockBody->getVelocity();
	ballVelocity.normalize();
	blockVelocity.normalize();
	contactBallBody->setVelocity(ballVelocity * velocity[0]);
	contactBlockBody->setVelocity(blockVelocity * velocity[1]);

	Node* nodeBall = contact.getShapeA()->getBody()->getNode();
	Node* nodeBlock = contact.getShapeB()->getBody()->getNode();

	if (nodeBall && nodeBlock) {
		if ((1 == contactBallBody->getCollisionBitmask() && 2 == contactBlockBody->getCollisionBitmask()) ||
			(2 == contactBallBody->getCollisionBitmask() && 1 == contactBlockBody->getCollisionBitmask())) {
			CCLOG("COLLISION WITH BLOCK");
			// cast Node to Block for using setLives()
			Block* contactBlockSprite = dynamic_cast<Block*>(nodeBlock);
			contactBlockSprite->setLives(contactBlockSprite->getLives() - 1);

			if (contactBlockSprite->getLives() == 0) {
				nodeBlock->removeFromParentAndCleanup(true);
				//add 10 scores for one block
				++this->_scores;
				this->_scoresLabel->setString(makeScoresLabel(this->_scores * 10));
				++this->_destroyCounter;
				//run winner scene if all blocks was destroyed
				if (_destroyCounter == 75) {
					auto youWin = YouWin::createScene();
					//set scores label in winner scene
					YouWin* win = dynamic_cast<YouWin*>(youWin);
					win->setWinScoresLabel(_scores);
					Director::getInstance()->replaceScene(TransitionFade::create(3.5, youWin, Color3B(255, 0, 0)));
				}
				//add to velocity of ball 10%
				if (_destroyCounter % 5 == 0) {
					auto ballVelocity = contactBallBody->getVelocity();
					contactBallBody->setVelocity(ballVelocity*1.1f);
					CCLOG("UP VELOCITY");
				}
			}
			else {
				contactBlockSprite->setTexture(contactBlockSprite->blockPictures[contactBlockSprite->getLives()]);
				contactBlockSprite->setContentSize(Size(visibleSize.width / 15, visibleSize.height / 30));
			}
		}
	}
	delete velocity;

	return true;
}

bool FirstLevel::onTouchBegan(Touch* touch, Event* event) {
	auto touchArea = touch->getLocation();
	auto spriteTouchArea = this->_playerSprite->getBoundingBox();
	if (spriteTouchArea.containsPoint(touchArea)) {
		CCLOG("touching the sprite");
		return true;
	}
	return false;
}

void FirstLevel::onTouchMoved(cocos2d::Touch* touch, cocos2d::Event* event) {
	auto visibleWidth = Director::getInstance()->getVisibleSize().width;
	auto playerSpriteSize = _playerSprite->getContentSize().width;

	_playerSprite->setPositionX(touch->getLocation().x);

	if (_playerSprite->getPositionX() >= visibleWidth - playerSpriteSize) {
		_playerSprite->setPositionX(visibleWidth - playerSpriteSize);
	}
	else if (_playerSprite->getPositionX() <= playerSpriteSize) {
		_playerSprite->setPositionX(playerSpriteSize);
	}
}

void FirstLevel::onTouchEnded(cocos2d::Touch* touch, cocos2d::Event* event) {

	CCLOG("End of touching");

}

