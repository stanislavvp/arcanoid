#include "GameOver.h"
#include "FirstLevel.h"
#include "toString.h"
USING_NS_CC;


Scene* GameOver::createScene() {

	return GameOver::create();

}

bool GameOver::init() {

	if (!Scene::init()) {
		return false;
	}

	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	//background
	auto background = DrawNode::create();
	background->drawSolidRect(origin, visibleSize, Color4F(0.0, 0.0, 0.0, 1.0));
	this->addChild(background, 0);

	//game over label
	auto gameLabel = Label::createWithTTF("GAME OVER", "fonts/STARWARS.ttf", 24);
	gameLabel->setTextColor(Color4B::YELLOW);
	gameLabel->setPosition(Vec2(visibleSize.width / 2 + origin.x, visibleSize.height / 1.5 + origin.y));
	this->addChild(gameLabel, 1);

	//Over Scores Label
	_overScoresLabel = Label::createWithTTF(makeScoresLabel(this->_overScores), "fonts/STARWARS.ttf", 24);
	_overScoresLabel->setTextColor(Color4B::YELLOW);
	_overScoresLabel->setPosition(Vec2(visibleSize.width / 2 + origin.x, visibleSize.height / 1.2 + origin.y));
	this->addChild(_overScoresLabel, 1);

	//button replay game
	auto startGameButton = MenuItemImage::create("replayNormal.png", "replaySelected.png", CC_CALLBACK_1(GameOver::restartCallback, this));
	startGameButton->setPosition(Vec2(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y));
	auto start = Menu::create(startGameButton, NULL);
	start->setPosition(Vec2::ZERO);
	this->addChild(start, 1);

	// button quit game
	auto closeGameButton = MenuItemImage::create("buttonQuitNormal.png", "buttonQuitSelected.png", CC_CALLBACK_1(GameOver::quitGameCallback, this));
	closeGameButton->setPosition(Vec2(visibleSize.width / 2 + origin.x, visibleSize.height / 3 + origin.y));
	auto close = Menu::create(closeGameButton, NULL);
	close->setPosition(Vec2::ZERO);
	this->addChild(close, 1);


	return true;
}

//start game method
void GameOver::restartCallback(Ref* pSender)
{
	auto startGame = FirstLevel::createScene();

	Director::getInstance()->replaceScene(TransitionFade::create(3.5, startGame, Color3B(255, 220, 0)));
}

//quit game method
void GameOver::quitGameCallback(Ref* pSender)
{
	Director::getInstance()->end();
}


void GameOver::setOverScoresLabel(int& scores) {
	_overScoresLabel->setString(makeScoresLabel(scores * 10));
}