#pragma once
#include "cocos2d.h"


class FirstLevel : public cocos2d::Scene {

private:

	cocos2d::PhysicsWorld* sceneWorld;

	void SetPhysicsWorld(cocos2d::PhysicsWorld* world) {
		sceneWorld = world;
	}

public:

	static cocos2d::Scene* createScene();

	virtual bool init();
	//ball collisions
	bool onContactSeparate(cocos2d::PhysicsContact &contact);

	bool onContactBegin(cocos2d::PhysicsContact &contact);
	//player control
	bool onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event);

	void onTouchMoved(cocos2d::Touch *touch, cocos2d::Event *event);

	void onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *event);


private:
	cocos2d::Sprite* _playerSprite;
	int _destroyCounter = 0;

	int _scores = 0;
	cocos2d::Label* _scoresLabel;

	CREATE_FUNC(FirstLevel);

};

