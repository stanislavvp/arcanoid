#pragma once
#include "cocos2d.h"

class GameMenu : public cocos2d::Scene {

public:

	static cocos2d::Scene* createScene();

	virtual bool init();

	void quitGameCallback(cocos2d::Ref* pSender);
	void startGameCallback(cocos2d::Ref* pSender);

	CREATE_FUNC(GameMenu);

};