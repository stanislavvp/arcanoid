#include "GameMenu.h"
#include "FirstLevel.h"

USING_NS_CC;


Scene* GameMenu::createScene() {

	return GameMenu::create();

}

bool GameMenu::init() {

	if (!Scene::init()) {
		return false;
	}

	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	//background
	auto background = DrawNode::create();
	background->drawSolidRect(origin, visibleSize, Color4F(0.0, 0.0, 0.0, 1.0));
	this->addChild(background, 0);

	//game label
	auto gameLabel = Label::createWithTTF("BRICKSTAR WARS", "fonts/STARWARS.ttf", 24);
	gameLabel->setTextColor(Color4B::YELLOW);
	gameLabel->setPosition(Vec2(visibleSize.width / 2 + origin.x, visibleSize.height / 1.5 + origin.y));
	this->addChild(gameLabel, 0);

	//button start game
	auto startGameButton = MenuItemImage::create("buttonNormal.png", "buttonSelected.png", CC_CALLBACK_1(GameMenu::startGameCallback, this));
	startGameButton->setPosition(Vec2(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y));
	auto start = Menu::create(startGameButton, NULL);
	start->setPosition(Vec2::ZERO);
	this->addChild(start, 1);

	// button quit game
	auto closeGameButton = MenuItemImage::create("buttonQuitNormal.png", "buttonQuitSelected.png", CC_CALLBACK_1(GameMenu::quitGameCallback, this));
	closeGameButton->setPosition(Vec2(visibleSize.width / 2 + origin.x, visibleSize.height / 3 + origin.y));
	auto close = Menu::create(closeGameButton, NULL);
	close->setPosition(Vec2::ZERO);
	this->addChild(close, 1);


	return true;
}

//start game method
void GameMenu::startGameCallback(Ref* pSender)
{
	auto startGame = FirstLevel::createScene();

	Director::getInstance()->replaceScene(TransitionFade::create(3.5, startGame, Color3B(255, 220, 0)));
}

//quit game method
void GameMenu::quitGameCallback(Ref* pSender)
{
	Director::getInstance()->end();
}
